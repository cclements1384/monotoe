﻿using System.Collections.Generic;
using MonoToe.Components;

namespace MonoToe.AI
{
    class GameAI
    {
        public static GamePiece GetComputerMove(GameSession gameSession,
            GameBoard myGameBoard)
        {
            var cpu = gameSession.CPUIcon.ToUpper();

            List<GamePiece> gameBoard = myGameBoard.gameBoard;

            GamePiece winningCell = null;
            GamePiece blockingCell = null;
            GamePiece playCell = null;

            /* can we win */
            winningCell = CanWinRow(ref gameBoard, cpu, "top");
            if (winningCell != null)
            {
                return winningCell;
            }

            winningCell = CanWinRow(ref gameBoard, cpu, "middle");
            if (winningCell != null)
            {
                return winningCell;
            }

            winningCell = CanWinRow(ref gameBoard, cpu, "bottom");
            if (winningCell != null)
            {
                return winningCell;
            }

            winningCell = CanWinColumn(ref gameBoard, cpu, "left");
            if (winningCell != null)
            {
                return winningCell;
            }

            winningCell = CanWinColumn(ref gameBoard, cpu, "middle");
            if (winningCell != null)
            {
                return winningCell;
            }

            winningCell = CanWinColumn(ref gameBoard, cpu, "right");
            if (winningCell != null)
            {
                return winningCell;
            }

            winningCell = CanWinDiagonal(ref gameBoard, cpu, "left");
            if (winningCell != null)
            {
                return winningCell;
            }

            winningCell = CanWinDiagonal(ref gameBoard, cpu, "right");
            if (winningCell != null)
            {
                return winningCell;
            }

            /* must we block */
            blockingCell = MustBlockRow(ref gameBoard, cpu, "top");
            if (blockingCell != null)
            {
                return blockingCell;
            }

            blockingCell = MustBlockRow(ref gameBoard, cpu, "middle");
            if (blockingCell != null)
            {
                return blockingCell;
            }

            blockingCell = MustBlockRow(ref gameBoard, cpu, "bottom");
            if (blockingCell != null)
            {
                return blockingCell;
            }

            blockingCell = MustBlockColumn(ref gameBoard, cpu, "left");
            if (blockingCell != null)
            {
                return blockingCell;
            }

            blockingCell = MustBlockColumn(ref gameBoard, cpu, "middle");
            if (blockingCell != null)
            {
                return blockingCell;
            }

            blockingCell = MustBlockColumn(ref gameBoard, cpu, "right");
            if (blockingCell != null)
            {
                return blockingCell;
            }

            blockingCell = MustBlockDiagonal(ref gameBoard, cpu, "left");
            if (blockingCell != null)
            {
                return blockingCell;
            }

            blockingCell = MustBlockDiagonal(ref gameBoard, cpu, "right");
            if (blockingCell != null)
            {
                return blockingCell;
            }

            /* can go in the center */
            playCell = CanPlayCenter(ref gameBoard, cpu);
            if (playCell != null)
            {
                return playCell;
            }

            /* can go in a corner */
            playCell = CanPlayCorner(ref gameBoard, cpu);
            if (playCell != null)
            {
                return playCell;
            }

            /* just make a play */
            playCell = CanPlayCenterRowOrColumn(ref gameBoard, cpu);
            if (playCell != null)
            {
                return playCell;
            }

            return null;
        }

        private static GamePiece CanWinRow(ref List<GamePiece> gameBoard, string cpuMark, string row)
        {
            var cpuCount = 0;
            var playerCount = 0;
            var emptyCount = 0;
            GamePiece winningCell = null;

            switch (row)
            {
                case "top":
                    {
                        for (var i = 0; i <= 2; i++)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                winningCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
                case "middle":
                    {
                        for (var i = 3; i <= 5; i++)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                winningCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
                case "bottom":
                    {
                        for (var i = 6; i <= 8; i++)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                winningCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
            }

            if (cpuCount == 2 && emptyCount == 1)
            {
                return winningCell;
            }
            return null;
        }

        private static GamePiece CanWinColumn(ref List<GamePiece> gameBoard, string cpuMark, string column)
        {
            var cpuCount = 0;
            var playerCount = 0;
            var emptyCount = 0;
            GamePiece winningCell = null;

            switch (column)
            {
                case "left":
                    {
                        for (var i = 0; i <= 6; i += 3)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                winningCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;

                case "middle":
                    {
                        for (var i = 1; i <= 7; i += 3)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                winningCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
                case "right":
                    {
                        for (var i = 2; i <= 8; i += 3)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                winningCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
            }
            if (cpuCount == 2 && emptyCount == 1)
            {
                return winningCell;
            }
            return null;
        }

        private static GamePiece CanWinDiagonal(ref List<GamePiece> gameBoard, string cpuMark, string direction)
        {
            var cpuCount = 0;
            var playerCount = 0;
            var emptyCount = 0;
            GamePiece winningCell = null;

            switch (direction)
            {
                case "right":
                    {
                        for (var i = 2; i <= 6; i += 4)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                winningCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;

                case "left":
                    {
                        for (var i = 0; i <= 8; i += 4)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                winningCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
            }
            if (cpuCount == 2 && emptyCount == 1)
            {
                return winningCell;
            }
            return null;
        }

        private static GamePiece MustBlockRow(ref List<GamePiece> gameBoard, string cpuMark, string row)
        {
            var cpuCount = 0;
            var playerCount = 0;
            var emptyCount = 0;
            GamePiece blockingCell = null;

            switch (row)
            {
                case "top":
                    {
                        for (var i = 0; i <= 2; i++)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                blockingCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
                case "middle":
                    {
                        for (var i = 3; i <= 5; i++)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                blockingCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
                case "bottom":
                    {
                        for (var i = 6; i <= 8; i++)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                blockingCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
            }

            if (playerCount == 2 && emptyCount == 1)
            {
                return blockingCell;
            }
            return null;
        }

        private static GamePiece MustBlockColumn(ref List<GamePiece> gameBoard, string cpuMark, string column)
        {
            var cpuCount = 0;
            var playerCount = 0;
            var emptyCount = 0;
            GamePiece blockingCell = null;

            switch (column)
            {
                case "left":
                    {
                        for (var i = 0; i <= 6; i += 3)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                blockingCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;

                case "middle":
                    {
                        for (var i = 1; i <= 7; i += 3)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                blockingCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
                case "right":
                    {
                        for (var i = 2; i <= 8; i += 3)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                blockingCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
            }
            if (playerCount == 2 && emptyCount == 1)
            {
                return blockingCell;
            }
            return null;
        }

        private static GamePiece MustBlockDiagonal(ref List<GamePiece> gameBoard, string cpuMark, string direction)
        {
            var cpuCount = 0;
            var playerCount = 0;
            var emptyCount = 0;
            GamePiece blockingCell = null;

            switch (direction)
            {
                case "right":
                    {
                        for (var i = 2; i <= 6; i += 2)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                blockingCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;

                case "left":
                    {
                        for (var i = 0; i <= 8; i += 2)
                        {
                            if (gameBoard[i].Owner == cpuMark)
                                cpuCount += 1;
                            else if (gameBoard[i].Owner == "")
                            {
                                emptyCount += 1;
                                blockingCell = gameBoard[i];
                            }
                            else
                                playerCount += 1;
                        }
                    }
                    break;
            }
            if (cpuCount == 2 && emptyCount == 1)
            {
                return blockingCell;
            }
            return null;
        }

        private static GamePiece CanPlayCenter(ref List<GamePiece> gameBoard, string cpuMark)
        {
            GamePiece playcell = null;

            if (gameBoard[4].IsEmpty)
            {
                playcell = gameBoard[4];
            }
            return playcell;

        }

        private static GamePiece CanPlayCorner(ref List<GamePiece> gameBoard, string cpuMark)
        {
            GamePiece playCell = null;

            /* top left */
            if (gameBoard[0].IsEmpty)
            {
                playCell = gameBoard[0];
                return playCell;
            }

            /* top right */
            if (gameBoard[2].IsEmpty)
            {
                playCell = gameBoard[2];
                return playCell;
            }

            /* bottom left */
            if (gameBoard[6].IsEmpty)
            {
                playCell = gameBoard[6];
                return playCell;
            }

            /* bottom right */
            if (gameBoard[8].IsEmpty)
            {
                playCell = gameBoard[8];
                return playCell;
            }
            return playCell;
        }

        private static GamePiece CanPlayCenterRowOrColumn(ref List<GamePiece> gameBoard, string cpuMark)
        {
            GamePiece playCell = null;

            /* top row */
            if (gameBoard[1].IsEmpty)
            {
                playCell = gameBoard[1];
                return playCell;
            }

            /* left column */
            if (gameBoard[3].IsEmpty)
            {
                playCell = gameBoard[3];
                return playCell;
            }

            /* right column */
            if (gameBoard[5].IsEmpty)
            {
                playCell = gameBoard[5];
                return playCell;
            }

            /* bottom row */
            if (gameBoard[7].IsEmpty)
            {
                playCell = gameBoard[7];
                return playCell;
            }
            return playCell;
        }
    }
}
