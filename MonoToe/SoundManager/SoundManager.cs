﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using System;
using System.Diagnostics;

namespace MonoToe.SoundManager
{
    public class SoundManager : IDisposable
    {
        private SoundEffect playerMoveSound;
        private SoundEffect cpuMoveSound;
        private Song buttonClick;
        private Song gameMusic;

        private SoundEffect hoverSound;
        private SoundEffectInstance hoverSoundInstance;
        private SoundEffectInstance buttonClickInstance;

        public void Init(ContentManager content)
        {
            try
            {
                gameMusic = content.Load<Song>("Sounds/dark factory");
                buttonClick = content.Load<Song>("sounds/buttonClick");
                hoverSound = content.Load<SoundEffect>("sounds/mouseOver");
                hoverSoundInstance = hoverSound.CreateInstance();
            }
            catch(Exception ex)
            {
                Debug.Write(String.Concat("Failed to load sound content.", ex.Message));
            }
        }

        public void StartGameMusic()
        {
            try
            {
                MediaPlayer.Play(gameMusic);
            }
            catch(ObjectDisposedException opex)
            {
                Debug.Write(String.Concat("Error starting game music.", opex));
            }
        }

        public void StopGameMusic()
        {
            try
            {
                MediaPlayer.Stop();
            }
            catch
            {
                Debug.Write("Error stopping game music.");
            }
        }

        public void PlayButtonHoverSound()
        {
            if (hoverSoundInstance.State != SoundState.Playing)
            {
                hoverSoundInstance.Play();
            }
            hoverSound.Play();
        }

        public void PlayButtonClickSound()
        {
            MediaPlayer.Play(buttonClick);
        }

        public void Dispose()
        {
           
        }
    }
}
