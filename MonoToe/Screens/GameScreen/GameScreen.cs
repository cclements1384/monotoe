﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoToe.ScreenManager;
using MonoToe.AI;
using MonoToe.Components;
using MonoToe.Rules;
using MonoToe.Input;
using System;
using Microsoft.Xna.Framework.Media;
using MonoToe.SoundManager;
using System.Diagnostics;
using MonoToe.Screens;

namespace MonoToe.GameScreens
{
    class GameScreen : Screen
    {
        #region Vars


        public enum GameState
        {
            PLAYER_READY_DELAY = 0,
            GAME_RUNNING = 1,
            END_OF_ROUND_DELAY = 2,
            GAME_OVER = 3
        }

        GameState gameState;
        GameSession _gameSession;

        Rectangle playerMoveRect;
        Rectangle hudRect;
        Rectangle gameScreenRect;

        GameBoard _gameBoard;

        Texture2D gameScreen;
        Texture2D hud;

        SpriteFont scoreFont;
        
        Vector2 playerScorePosition;
        Vector2 cpuScorePosition;
        Vector2 catScorePosition;
        
        int playerScore;
        int cpuScore;
        int catScore;

        bool _gameOver = false;
        double _gameOverTime;
        double _newGamePause;
        int _pause = 3;
       
        Texture2D frame;

        /* used in pausing the game between rounds. */
        bool pausing = true;
        double startPauseTime = 0;
        int pauseDelayInSeconds = 2;
        float countDownScale = 1.0f;
        float maxCountDownScale = 3.0f;
        float countDownScaleSpeed = 0.05f;
        Vector2 countDownPosition;

        /* used to draw the score */
        float scale = 1.0f;
        int scoreYPosition = 60;
        int playerScoreXPosition = 200;
        int cpuScoreXPosition = 580;
        int catScoreXPosition = 730;

        /* animating */
        bool readyAnimating = true;
        bool gameOverAnimating = false;

        FontScaler playerReadyFontScaler;
        ScreenPause endOfRoundPauser;

#endregion

        public GameScreen(Game game,
          GraphicsDevice device,
          ContentManager content, 
          SoundManager.SoundManager soundManger,
          GameSession gameSession = null) 
            :base(game,device,content,"gameScreen", soundManger)
        {
            //_content = content;
            _gameSession = gameSession;
            _gameBoard = new GameBoard(game,_content);
        }

        public override bool Init()
        {
         

            _gameBoard.Init();

            hudRect = new Rectangle(0, 0, GetClientBounds().Width, 99);
            gameScreenRect = new Rectangle(0, 0,
              _game.Window.ClientBounds.Width,
              _game.Window.ClientBounds.Height);

            _game.Window.ClientSizeChanged += OnClientSizeChanged;

            _gameOverTime = 0;
            _newGamePause = 0;
            _gameOver = false;

            playerScore = 0;
            cpuScore = 0;
            catScore = 0;

            /* set the initial position of the scores */
            RepositionScores(new Rectangle(0, 0,
             _game.Window.ClientBounds.Width,
             _game.Window.ClientBounds.Height));

            countDownPosition = new Vector2(_game.Window.ClientBounds.Width / 2,
                _game.Window.ClientBounds.Height / 2);

            playerReadyFontScaler = new FontScaler(
                1.0f,  // start scale
                2.5f,  // max scale
                0.1f, // scale speed
                new Vector2(_game.Window.ClientBounds.Width / 2,
                _game.Window.ClientBounds.Height / 2),
                2, // delay
                "Get Ready!");

            /* set the initial state of the game. */
            gameState = GameState.PLAYER_READY_DELAY;
            endOfRoundPauser = new ScreenPause();
            endOfRoundPauser.DelaySeconds = 3;

            /* play game music */
            _soundManager.Init(_content);
            _soundManager.StartGameMusic();
            return true;
        }
        
        public override void LoadContent()
        {
            _gameBoard.LoadContent();
            scoreFont = _content.Load<SpriteFont>("fonts/score");
            gameScreen = _content.Load<Texture2D>("graphics/gamescreen");
            hud = _content.Load<Texture2D>("graphics/overlay");
            playerReadyFontScaler.LoadContent(scoreFont);
        }

        public override void UnloadContent()
        {
            _gameBoard.UnloadContent();
            _content.Unload();
            hud.Dispose();
            base.UnloadContent();
            GC.Collect();
        }

        public override void Update(GameTime gameTime)
        {
            if (_gameSession == null ||
                _gameBoard == null)
                return;

            switch (gameState)
            {
                case GameState.PLAYER_READY_DELAY:
                    {
                        playerReadyFontScaler.Update(gameTime);
                        if (!playerReadyFontScaler.Animating)
                            gameState = GameState.GAME_RUNNING;
                    }break;
                case GameState.GAME_RUNNING:
                    {
                        if (!_gameSession.IsGameOver(_gameBoard))
                        {
                            UpdateGameBoard();
                            DoGameTurn(gameTime);
                        }
                        else
                        {
                            gameState = GameState.END_OF_ROUND_DELAY;
                        }
                    }
                    break;
                case GameState.END_OF_ROUND_DELAY:
                    {
                        endOfRoundPauser.Pause(gameTime);
                        if(!endOfRoundPauser.IsPaused(gameTime))
                            gameState = GameState.GAME_OVER;
                    }break;
                case GameState.GAME_OVER:
                    {
                        DoEndGame(gameTime);
                    }
                    break;
                default:break;
            }
            
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
          
            _spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, null);
            
            /* draw game board */
            _gameBoard.Draw(_spriteBatch, _game);

            /* draw scores */
            DrawScores();

            /* draw the animated ready font */
            if (gameState == GameState.PLAYER_READY_DELAY)
                playerReadyFontScaler.Draw(_spriteBatch);
      
            _spriteBatch.End();

            base.Draw(gameTime);
        }

        private void DrawPlayerReady(SpriteBatch spriteBatch)
        {
            var countDown = "Ready!";

            // Find the center of the string
            Vector2 FontOrigin = new Vector2(scoreFont.MeasureString(countDown).X / 2,
                scoreFont.MeasureString(countDown).Y / 2);

            //// Draw the string
            _spriteBatch.DrawString(scoreFont,
                countDown,
                countDownPosition,
                new Color(91, 235, 0),//Monotoe green
                0,
                FontOrigin,
                countDownScale,
                SpriteEffects.None,
                0.5f);

            if(countDownScale <= maxCountDownScale)
                countDownScale += countDownScaleSpeed;
            ;
        }
        
        private void ResetGame()
        {
            _gameOver = false;
            _gameOverTime = 0;
            SCREEN_MANAGER.UnloadContent();
            SCREEN_MANAGER.goto_screen("gameOver");
            SCREEN_MANAGER.LoadContent();
        }

        //private bool Pause(GameTime gameTime, double pauseStartTime)
        //{
        //    if (pauseStartTime != 0)
        //        return gameTime.TotalGameTime.TotalSeconds < (_pause + pauseStartTime);
        //    else
        //        return false;
        //}

        private void DrawOverlay()
        {
            _spriteBatch.Draw(hud, gameScreenRect, Color.White);
        }

        private void DrawScores()
        {

            var playerScore = string.Format("Player: {0}", _gameSession.PlayerScore);
            var cpuScore = string.Format("Cpu: {0}", _gameSession.CPUScore);
            var drawScore = string.Format("Draw: {0}", _gameSession.DrawScore);

            DrawScore(playerScorePosition, playerScore);
            DrawScore(cpuScorePosition, cpuScore);
            DrawScore(catScorePosition, drawScore);
        }

        private void DrawScore(Vector2 position, string score)
        {

            // Find the center of the string
            Vector2 FontOrigin = new Vector2(scoreFont.MeasureString(score.ToString()).X / 2,
                scoreFont.MeasureString(score.ToString()).Y / 2);
           
          
            //// Draw the string
            _spriteBatch.DrawString(scoreFont,
                score.ToString(),
                position,
                new Color(91, 235, 0),//Monotoe green
                0, 
                FontOrigin,
                scale, 
                SpriteEffects.None, 
                0.5f);
            
        }
        
        private void OnClientSizeChanged(object sender, EventArgs e)
        {
            gameScreenRect = new Rectangle(0, 0,
             _game.Window.ClientBounds.Width,
             _game.Window.ClientBounds.Height);

            _gameBoard.RepositionGameBoard(_game.Window.ClientBounds);
            RepositionScores(gameScreenRect);
        }

        private GamePiece GetPlayerMove()
        {
            var mouseState = Mouse.GetState();
            return GameScreenMouseHandler.GetPlayerMove(mouseState,
                _gameBoard);
        }

        private GamePiece GetCPUMove(GameTime gameTime)
        {
            if (!_gameBoard.IsThinking(gameTime))
            {
                return GameAI.GetComputerMove(_gameSession,
                _gameBoard);
            }
            else
            {
                return null;
            }
        }

        private GamePiece GetNextMove(GameTime gameTime)
        {
            if (_gameSession.PlayerTurn)
            {
                return GetPlayerMove();
            }
            else
            {
                return GetCPUMove(gameTime);
            }
        }

        private void DoGameTurn(GameTime gameTime)
        {
            var nextMove = GetNextMove(gameTime);

            if (nextMove != null)
            {
                /* update the game board */
                _gameBoard.Update(_gameSession, nextMove);

                /* swap the game turn */
                _gameSession.NextPlayerTurn();

                if (_gameSession.CPUTurn)
                {
                    _gameBoard.StartThink(gameTime);
                }
            }
        }

        private void DoEndGame(GameTime gameTime)
        {
            if (_gameOverTime == 0)
            {
                /* Display some message that indicates the winner */
                _gameOver = true;
                _gameOverTime = gameTime.TotalGameTime.TotalSeconds;
                
            }

            if (_gameOver)
            {
                /* get the winner and update the score */
                _gameSession.UpdateScore(_gameBoard);

                /* stop the music */
                _soundManager.StopGameMusic();

                /* reset pausing variables */
                pausing = true;
                startPauseTime = 0;
                countDownScale = 1.0f;
                
                /* show game over screen */
                SCREEN_MANAGER.UnloadContent();
                SCREEN_MANAGER.goto_screen("gameOver");
                SCREEN_MANAGER.LoadContent();
            }
        }
      
        private void RepositionScores(Rectangle rect)
        {
            playerScorePosition = new Vector2(rect.Left + playerScoreXPosition,
                scoreYPosition);
            cpuScorePosition = new Vector2(rect.Right - 200, scoreYPosition);
            catScorePosition = new Vector2(rect.Width /2 - 10, scoreYPosition);
        }

        private void UpdateGameBoard()
        {
            foreach(var piece in _gameBoard.gameBoard)
            {
                piece.Update();
            }
        }

        private void ContinuePause(GameTime gameTime)
        {
            /* set the start pause time */
            if (startPauseTime == 0)
                startPauseTime = gameTime.TotalGameTime.TotalSeconds;

            /* check to see if the elesped time is > the pause delay. */
            if ((gameTime.TotalGameTime.TotalSeconds - startPauseTime) > pauseDelayInSeconds)
                pausing = false;
        }
    }
}
