﻿using Microsoft.Xna.Framework.Content;
using MonoToe.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoToe.Screens.GameOver
{
    public class YesButton : Button
    {
        private bool lastStateHover = false;

        public YesButton(ContentManager content,
            SoundManager.SoundManager soundManger) 
            : base(content,soundManger)
        {

        }

        public override void LoadContent()
        {
            _buttonTexture = _content.Load<Texture2D>("graphics/yes");
            _buttonTextureHover = _content.Load<Texture2D>("graphics/yeshover");
            base.LoadContent();
        }

        public override void Reposition(Rectangle clientBounds)
        {
            /* resposition this button */
            var x = clientBounds.Width / 2 - Width;
            var y = clientBounds.Height / 2 - Height + 100;
            Position = new Vector2(x, y);

            base.Reposition(clientBounds);
        }

        public override void Update(GameTime gameTime, MouseState mouse)
        {
            if (_hover)
            {
                if (!lastStateHover)
                {
                    lastStateHover = true;
                    _soundManager.PlayButtonHoverSound();
                }
            }
            else
            {
                lastStateHover = false;
            }
            base.Update(gameTime, mouse);
        }
    }
}
