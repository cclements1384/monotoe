﻿using Microsoft.Xna.Framework.Content;
using MonoToe.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoToe.Screens.GameOver
{
    public class NoButton : Button
    {
        private bool lastStateHover = false;
        
        public NoButton(ContentManager content,
            SoundManager.SoundManager soundManager)
            : base(content,soundManager)
        {

        }

        public override void LoadContent()
        {
            _buttonTexture = _content.Load<Texture2D>("graphics/no");
            _buttonTextureHover = _content.Load<Texture2D>("graphics/nohover");
            base.LoadContent();
        }

        public override void Reposition(Rectangle clientBounds)
        {
            /* this updates the position of the no button */
            var x = clientBounds.Width / 2 + 20;
            var y = clientBounds.Height / 2 - Height + 100;
            Position = new Vector2(x, y);
           
            base.Reposition(clientBounds);
        }

        public override void Update(GameTime gameTime, MouseState mouse)
        {
            if (_hover)
            {
                if (!lastStateHover)
                {
                    lastStateHover = true;
                    _soundManager.PlayButtonHoverSound();
                }
            }
            else
            {
                lastStateHover = false;
            }
            base.Update(gameTime, mouse);
        }
    }
}
