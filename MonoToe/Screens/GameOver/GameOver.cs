﻿using MonoToe.ScreenManager;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using MonoToe.UI;
using MonoToe.Screens.GameOver;

namespace MonoToe.GameScreens
{
    class GameOver : Screen
    {
        private Texture2D gameOver;
        private Rectangle gameOverRect;
        
        Button yesButton;
        Button noButton;
  
        public GameOver(Game game,GraphicsDevice device,
           ContentManager content,
           SoundManager.SoundManager soundManager) 
            :base(game,device,content,"gameOver",soundManager)
        {

        }

        public override bool Init()
        {
            gameOverRect = GetClientBounds();
            /* handler for screen res changes */
            _game.Window.ClientSizeChanged += OnWindowResized;
            /* Init the yes button */
            yesButton = new YesButton(_content,_soundManager);
            yesButton.ButtonPressed += OnYesButtonPress;
            /* Init the no button */
            noButton = new NoButton(_content,_soundManager);
            noButton.ButtonPressed += OnNoButtonPress;
            return true;
        }

        private void OnWindowResized(object sender, EventArgs e)
        {
            /* Get the client bounds */
            var clientBounds = GetClientBounds();
            /* Recalculate the background destination rectangle */
            gameOverRect = clientBounds;
            /* reset the location of the yes button */
            yesButton.Reposition(clientBounds);
            /* reset the lication of the no button */
            noButton.Reposition(clientBounds);
        }

        private void OnNoButtonPress(object sender, EventArgs e)
        {
            SCREEN_MANAGER.goto_screen("gameMenu");
            SCREEN_MANAGER.LoadContent();
        }

        private void OnYesButtonPress(object sender, EventArgs e)
        {
            SCREEN_MANAGER.go_back();
            SCREEN_MANAGER.LoadContent();
        }

        public override void LoadContent()
        {
            gameOver = _content.Load<Texture2D>("graphics/gameover");
            yesButton.LoadContent();
            noButton.LoadContent();
            /* I have to do this here because the textures 
             * have to be in before we can position the 
             * buttons */
            var clientBounds = GetClientBounds();
            yesButton.Reposition(clientBounds);
            noButton.Reposition(clientBounds);
        }
        
        public override void Update(GameTime gameTime)
        {
            var keys = Keyboard.GetState();
            var mouse = Mouse.GetState();
            yesButton.Update(gameTime, mouse);
            noButton.Update(gameTime, mouse);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, null);
            _spriteBatch.Draw(gameOver, gameOverRect, Color.White);
            yesButton.Draw(_spriteBatch);
            noButton.Draw(_spriteBatch);
            _spriteBatch.End();
            base.Draw(gameTime);
        }

        public override void Shutdown()
        {
            _content.Unload();
            base.Shutdown();
        }
    }
}
