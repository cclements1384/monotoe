﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoToe.ScreenManager;
using Microsoft.Xna.Framework.Input;
using System;
using MonoToe.Screens.GameMenu;

namespace MonoToe.GameScreens.GameMenu
{
    class GameMenu : Screen
    {
        Texture2D gameMenu;
        SpriteFont playerScore;
        Rectangle gameMenuRect;
        NewGameButton newGameButton;
        ExitGameButton exitGameButton;
      
        public GameMenu(Game game,GraphicsDevice device,
          ContentManager content,
          SoundManager.SoundManager soundManager) 
            :base(game,device,content,"gameMenu",
                 soundManager)
        {
            
        }

        public override bool Init()
        {
            gameMenuRect = GetClientBounds();
            // new game button
            newGameButton = new NewGameButton(_content, 
                _soundManager);
            // button clicked event
            newGameButton.ButtonPressed += NewGameButtonPressed;
            _game.Window.ClientSizeChanged += ScreenResized;
            // quit button
            exitGameButton = new ExitGameButton(_content, 
                _soundManager);
            // quit button event
            exitGameButton.ButtonPressed += ExitGameButtonPressed;

            /* play game music */
            //_soundManager.StartGameMusic();

            return true;
        }
        
        private void ExitGameButtonPressed(object sender, EventArgs e)
        {
            App.Current.Exit();
        }
        
        private void ScreenResized(object sender, EventArgs e)
        {
            /* Adjust the size of the texture destination retangle */
            gameMenuRect = GetClientBounds();
            /* Adjust the position of the button */
            newGameButton.Reposition(gameMenuRect);
            /* Adjust the position of the button */
            exitGameButton.Reposition(gameMenuRect);
        }

        private void NewGameButtonPressed(object sender, EventArgs e)
        {
            SCREEN_MANAGER.UnloadContent();
            SCREEN_MANAGER.goto_screen("gameScreen");
            SCREEN_MANAGER.LoadContent();
        }

        public override void LoadContent()
        {
            gameMenu = _content.Load<Texture2D>("graphics/gamemenu");
            playerScore = _content.Load<SpriteFont>("fonts/score");
            newGameButton.LoadContent();
            exitGameButton.LoadContent();
            /* Do this here so that we have the texture 
             * loaded to set the screen */
            var clientBounds = GetClientBounds();
            newGameButton.Reposition(clientBounds);
            exitGameButton.Reposition(clientBounds);
        }
                
        public override void Update(GameTime gameTime)
        {
            var mouse = Mouse.GetState();
            newGameButton.Update(gameTime, mouse);
            exitGameButton.Update(gameTime, mouse);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, null);
            _spriteBatch.Draw(gameMenu, gameMenuRect, Color.White);
            newGameButton.Draw(_spriteBatch);
            exitGameButton.Draw(_spriteBatch);
            _spriteBatch.End();
            base.Draw(gameTime);
        }

        public override void Shutdown()
        {
            _content.Unload();
            base.Shutdown();
        }

    }
}
