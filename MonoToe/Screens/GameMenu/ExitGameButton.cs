﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoToe.UI;

namespace MonoToe.Screens.GameMenu
{
    public class ExitGameButton : Button
    {
        SoundManager.SoundManager soundManger;
        private bool lastStateHover = false;


        public ExitGameButton(ContentManager content,
            SoundManager.SoundManager soundManager)
            : base(content,soundManager)
        {

        }

        public override void LoadContent()
        {
            _buttonTexture = _content.Load<Texture2D>("graphics/quit");
            _buttonTextureHover = _content.Load<Texture2D>("graphics/quithover");
            base.LoadContent();
        }

        public override void Reposition(Rectangle clientBounds)
        {
            var x = clientBounds.Width / 2 - Width / 2;
            var y = clientBounds.Height / 2 - Height / 2 + 75;
            Position = new Vector2(x, y);

            base.Reposition(clientBounds);
        }

        public override void Update(GameTime gameTime, MouseState mouse)
        {
            if (_hover)
            {
                if (!lastStateHover)
                {
                    lastStateHover = true;
                    _soundManager.PlayButtonHoverSound();
                }
            }
            else
            {
                lastStateHover = false;
            }
            base.Update(gameTime, mouse);
        }
    }
}
