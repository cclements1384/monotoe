﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoToe.UI;
using System;

namespace MonoToe.GameScreens.GameMenu
{
    public class NewGameButton : Button
    {
        private bool lastStateHover = false;

        public NewGameButton(ContentManager content,
            SoundManager.SoundManager soundManager)
        : base(content, soundManager)
        {
        }

        public override void LoadContent()
        {
            _buttonTexture = _content.Load<Texture2D>("graphics/start-1");
            _buttonTextureHover = _content.Load<Texture2D>("graphics/start-2");
            base.LoadContent();
        }
        
        public override void Reposition(Rectangle clientBounds)
        {
            var x = clientBounds.Width / 2 - Width / 2;
            var y = clientBounds.Height / 2 - Height / 2;
            Position = new Vector2(x, y);
            base.Reposition(clientBounds);
        }

        public override void Update(GameTime gameTime, MouseState mouse)
        {
            if (_hover)
            {
                if (!lastStateHover)
                {
                    lastStateHover = true;
                    _soundManager.PlayButtonHoverSound();
                }
            }
            else
            {
                lastStateHover = false;
            }
            base.Update(gameTime, mouse);
        }


    } 
}
