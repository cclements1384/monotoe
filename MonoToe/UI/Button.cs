﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;

namespace MonoToe.UI
{
    public class Button
    {
        protected ContentManager _content;
        protected SoundManager.SoundManager _soundManager;
        protected Game _game;

        protected Rectangle _location;

        protected Texture2D _buttonTexture;
        protected Texture2D _buttonTextureHover;

        protected int _lastTouchId;
        protected bool _pressed;
        protected bool _hover;
        protected Vector2 _postion;

        private Rectangle Location
        {
            get
            {
                if (_buttonTexture == null || _postion == null)
                    return _location;
                _location = new Rectangle((int)_postion.X,
                                         (int)_postion.Y,
                                        _buttonTexture.Width,
                                        _buttonTexture.Height);
                return _location;
            }
        }

        public Vector2 Position
        {
            set
            {
                _postion = value;

                if (_buttonTexture != null)
                    _location = new Rectangle((int)value.X,
                                         (int)value.Y,
                                        _buttonTexture.Width,
                                        _buttonTexture.Height);
            }
            get
            {
                return _postion;
            }
        }

        public int Width
        {
            get { return _buttonTexture.Width; }
        }
        public int Height
        {
            get { return _buttonTexture.Height; }
        }

        public Button(ContentManager content,
            SoundManager.SoundManager soundManager)
        {
            _content = content;
            _soundManager = soundManager;
        }

        public virtual void LoadContent()
        {

        }
        
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (_hover)
                spriteBatch.Draw(_buttonTextureHover,
                Location,
                null,
                Color.White);
            else
                spriteBatch.Draw(_buttonTexture,
               Location,
               null,
               Color.White);
        }

        public virtual void Update(GameTime gameTime, MouseState mouse)
        {
            IsHover(mouse);
            IsPressed(mouse);
        }

        public virtual void Reposition(Rectangle clientBounds)
        {

        }

        public virtual bool IsPressed(ref TouchCollection touches)
        {
            foreach (var touch in touches)
            {
                if (touch.Id == _lastTouchId)
                {
                    continue;
                }

                if (touch.State != TouchLocationState.Pressed)
                    continue;
                if (_location.Contains(touch.Position))
                {
                    _lastTouchId = touch.Id;
                    _pressed = true;
                    return true;
                }
            }
            _pressed = false;
            return false;
        }

        public virtual bool IsPressed(MouseState mouse)
        {

            var leftButton = mouse.LeftButton;
            if (leftButton == ButtonState.Pressed && _location.Contains(mouse.Position))
            {
                OnPressed(EventArgs.Empty);
                return true;
            }
            return false;
        }

        public virtual bool IsHover(MouseState mouse)
        {
            var position = mouse.Position;
            _hover = false;

            if (_location.Contains(position))
            {
                _hover = true;
            }
            return _hover;
        }

        public virtual void OnPressed(EventArgs e)
        {
            EventHandler handler = ButtonPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler ButtonPressed;

    }
}
