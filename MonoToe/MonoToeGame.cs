﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoToe.GameScreens;
using MonoToe.ScreenManager;
using MonoToe.Components;
using MonoToe.GameScreens.GameMenu;

namespace MonoToe
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MonoToeGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SoundManager.SoundManager soundManager;

        public MonoToeGame()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.ToggleFullScreen();

            this.IsMouseVisible = true;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            var gameSession = new GameSession();

            /* Init the sound manager class */
            soundManager = new SoundManager.SoundManager();
            soundManager.Init(Content);
                     
            /* Init our screen manager and add a screens to it. */
            SCREEN_MANAGER.add_screen(new GameMenu(this, 
                GraphicsDevice, 
                Content,
                soundManager));
            SCREEN_MANAGER.add_screen(new GameScreen(this,
                GraphicsDevice, 
                Content,
                soundManager,
                gameSession));
            SCREEN_MANAGER.add_screen(new GameOver(this,
                GraphicsDevice, 
                Content,
                soundManager));
        
            /* Set the active screen to the game screen */
            SCREEN_MANAGER.goto_screen("gameMenu");

            /* Init the current screen */
            SCREEN_MANAGER.Init();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            /* Have the active screen initilize itself. */
            SCREEN_MANAGER.LoadContent();

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            SCREEN_MANAGER.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            SCREEN_MANAGER.Draw(gameTime);
         
            base.Draw(gameTime);
        }
      
    }
}
