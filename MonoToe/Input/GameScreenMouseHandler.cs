﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MonoToe.Components;

namespace MonoToe.Input
{
    public class GameScreenMouseHandler
    {
        public static GamePiece GetPlayerMove(MouseState mouseState, GameBoard _gameBoard)
        {
            var leftButtonState = mouseState.LeftButton;
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                var x = mouseState.X;
                var y = mouseState.Y;

                var point = new Point(x, y);

                foreach (var cell in _gameBoard.gameBoard)
                {
                    if (cell.IsEmpty)
                        if (cell.Contains(point))
                        {
                            return cell;
                        }
                }
            }
            return null;
        }
    }
}
