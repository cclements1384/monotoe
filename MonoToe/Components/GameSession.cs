﻿using MonoToe.Rules;

namespace MonoToe.Components
{
    public class GameSession
    {
        private bool _cpuTurn;
        private bool _playerTurn;

        public int PlayerScore { get; set; }
        public int CPUScore { get; set; }
        public int DrawScore { get; set; }
        public bool CPUTurn
        {
            get
            {
                return _cpuTurn;
            }
            set
            {
                _playerTurn = !value;
                _cpuTurn = value;
            }
        }
        public bool PlayerTurn
        {
            get
            {
                return _playerTurn;
            }
            set
            {
                _cpuTurn = !value;
                _playerTurn = value;
            }
        }

        public string PlayerIcon { get; set; }
        public string CPUIcon { get; set; }
        public string CatIcon { get; set; }
       
        public GameSession()
        {
            /* default values */
            PlayerScore = 0;
            CPUScore = 0;
            DrawScore = 0;
            PlayerIcon = "X";
            CPUIcon = "O";
            CatIcon = "DRAW";
            PlayerTurn = true;
        }

        public void NextPlayerTurn()
        {
            PlayerTurn = !PlayerTurn;
        }

        public void UpdateScore(GameBoard gameBoard)
        {
            if (gameBoard == null)
                return;

            if (GameOverRule.IsGameOver(gameBoard))
            {
                var winner = GameWinnerRule.GetWinningPlayer(gameBoard);

                if (winner == "PLAYER")
                    PlayerScore += 1;
                else if (winner == "CPU")
                    CPUScore += 1;
                else
                    DrawScore += 1;
            }
        }

        public void ResetGame(GameBoard gameBoard)
        {
            gameBoard.ResetGameBoard();
        }

        public bool IsGameOver(GameBoard gameBoard)
        {
            return GameOverRule.IsGameOver(gameBoard);
        }
    }
}
