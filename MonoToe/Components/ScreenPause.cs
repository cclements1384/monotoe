﻿using Microsoft.Xna.Framework;
using System;

namespace MonoToe.Components
{
    public class ScreenPause
    {
        private int _seconds;
        private double _startPauseSeconds;
        public int DelaySeconds
        {
            get
            {
                return _seconds;
            }
            set
            {
                _seconds = value;
            }
        }
                
        public void Pause(GameTime gameTime)
        {
            if (_seconds == 0)
                throw new ArgumentException("You must define the delay in seconds using the DelaySeconds property.");
            if (_startPauseSeconds == 0)
                _startPauseSeconds = gameTime.TotalGameTime.TotalSeconds;
        }

        public void UnPause()
        {
            _seconds = 0;
            _startPauseSeconds = 0;
        }

        public bool IsPaused(GameTime gameTime)
        {
            return (gameTime.TotalGameTime.TotalSeconds - _startPauseSeconds) <= _seconds;
        }
    }
}
