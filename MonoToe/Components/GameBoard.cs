﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Text.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MonoToe.Components
{
    public class GameBoard
    {
        ContentManager _content;
        GameSession _gameSession;
        private List<GamePiece> _gameBoard;
        Game _game;

        Rectangle gameboardRect;
        Rectangle topLeftRect;
        Rectangle topCenterRect;
        Rectangle topRightRect;
        Rectangle centerLeftRect;
        Rectangle centercenterRect;
        Rectangle centerRightRect;
        Rectangle bottomLeftRect;
        Rectangle bottomCenterRect;
        Rectangle bottomRightRect;

        Texture2D gameBoardText;
        Texture2D playerTexture;
        Texture2D cpuTexture;

        private bool _isThinking = false;
        private double _seconds;
        private int _timeToThink = 1;

        private const string OWNER_PLAYER = "PLAYER";
        private const string OWNER_CPU = "CPU";
        private const string OWNER_EMPTY = "";
      
        public List<GamePiece> gameBoard
        {
            get
            {
                return _gameBoard;
            }
        }

        public GameBoard(Game game,ContentManager content)
        {
            _content = content;
            _game = game;
        }

        public void Init()
        {
        }

        public void LoadContent()
        {
            gameBoardText = _content.Load<Texture2D>("graphics/gameboard");
            playerTexture = _content.Load<Texture2D>("graphics/x");
            cpuTexture = _content.Load<Texture2D>("graphics/o");
            InitGameBoard();
        }

        public void Draw(SpriteBatch spriteBatch, Game game)
        {
            DrawGameBoard(spriteBatch);
            DrawGamePieces(spriteBatch);
        }

        public void Update(GameSession gameSession, GamePiece GamePiece)
        {
            /* Update the gameBoard */
            var index = _gameBoard.IndexOf(GamePiece);

            UpdateGamePieces();


            if (gameSession.PlayerTurn)
            {
                GamePiece.Texture = playerTexture;
                GamePiece.Owner = OWNER_PLAYER ;
            }
            else
            {
                GamePiece.Texture = cpuTexture;
                GamePiece.Owner = OWNER_CPU;
            }
            _gameBoard[index] = GamePiece;
        }

        public void UnloadContent()
        {
            _content.Unload();
        }

        public void ResetGameBoard()
        {
            /* TODO: pass in the game instance here. */
            InitGameBoard();
        }
      
        private void InitGameBoard()
        {
            _gameBoard = new List<GamePiece>();

            var width = _game.Window.ClientBounds.Width;
            var height = _game.Window.ClientBounds.Height;
            var gameboardX = width / 2 - gameBoardText.Width / 2;
            var gameboardY = height / 2 - gameBoardText.Height / 2;

            gameboardRect = new Rectangle(gameboardX, gameboardY, gameBoardText.Width, gameBoardText.Height);

            var playerWidth = 138;
            var playerHeight = 137;
            var leftPadding = 32;
            var topPadding = 22;
            var xOffset = leftPadding / 2;
            var yOffset = topPadding / 2;

            /* top left */
            topLeftRect = new Rectangle(gameboardRect.X + xOffset, gameboardRect.Y + yOffset, playerWidth, playerHeight);
            gameBoard.Add(new GamePiece(topLeftRect, OWNER_EMPTY));

            /* top center */
            var left = gameboardRect.X + playerWidth + leftPadding + leftPadding;
            var y = gameboardRect.Y + yOffset;
            topCenterRect = new Rectangle(left, y, playerWidth, playerHeight);
            gameBoard.Add(new GamePiece(topCenterRect, OWNER_EMPTY));

            /* top right */
            left = left + playerWidth + leftPadding + xOffset;
            y = gameboardRect.Y + yOffset;
            topRightRect = new Rectangle(left, y, playerWidth, playerHeight);
            gameBoard.Add(new GamePiece(topRightRect, OWNER_EMPTY));

            /* center left */
            y = gameboardRect.Y + playerHeight + topPadding + topPadding + yOffset;
            centerLeftRect = new Rectangle(gameboardRect.X + xOffset, y + yOffset, playerWidth, playerHeight);
            gameBoard.Add(new GamePiece(centerLeftRect, OWNER_EMPTY));

            /* center center */
            left = gameboardRect.X + playerWidth + leftPadding + xOffset + xOffset;
            centercenterRect = new Rectangle(left, y+ yOffset, playerWidth, playerHeight);
            gameBoard.Add(new GamePiece(centercenterRect, OWNER_EMPTY));

            /* center right */
            left = left + playerWidth + leftPadding + xOffset;
            centerRightRect = new Rectangle(left, y + yOffset, playerWidth, playerHeight);
            gameBoard.Add(new GamePiece(centerRightRect,OWNER_EMPTY));

            /* bottom left */
            y = gameboardRect.Y + (playerHeight * 2) + (topPadding * 5) + yOffset ;
            bottomLeftRect = new Rectangle(gameboardRect.X + xOffset, y, playerWidth, playerHeight);
            gameBoard.Add(new GamePiece(bottomLeftRect, OWNER_EMPTY));

            /* bottom center */
            left = gameboardRect.X + playerWidth + leftPadding + xOffset + xOffset;
            bottomCenterRect = new Rectangle(left, y, playerWidth, playerHeight);
            gameBoard.Add(new GamePiece(bottomCenterRect, OWNER_EMPTY));

            /* bottom right */
            left = left + playerWidth + leftPadding + xOffset;
            bottomRightRect = new Rectangle(left, y, playerWidth, playerHeight);
            gameBoard.Add(new GamePiece(bottomRightRect, OWNER_EMPTY));
        }

        public void StartThink(GameTime gameTime)
        {
            _isThinking = true;
            _seconds = gameTime.TotalGameTime.TotalSeconds;
        }

        public bool IsThinking(GameTime gameTime)
        {
            if (gameTime.TotalGameTime.TotalSeconds >= _seconds + _timeToThink)
                return false;
            return true;
        }

        public void RepositionGameBoard(Rectangle rectangle)
        {
            var width = rectangle.Width;
            var height = rectangle.Height;
            var gameboardX = width / 2 - gameBoardText.Width / 2;
            var gameboardY = height / 2 - gameBoardText.Height / 2;

            gameboardRect = new Rectangle(gameboardX, gameboardY, gameBoardText.Width, gameBoardText.Height);

            RepositionMoves(gameboardRect);
        }

        private void RepositionMoves(Rectangle rect)
        {
            var playerWidth = 138;
            var playerHeight = 137;
            var leftPadding = 32;
            var topPadding = 22;
            var xOffset = leftPadding / 2;
            var yOffset = topPadding / 2;
            
            /* top left */
            topLeftRect = new Rectangle(gameboardRect.X + xOffset, gameboardRect.Y + yOffset, playerWidth, playerHeight);
            gameBoard[0].Rectangle = topLeftRect;

            /* top center */
            var left = gameboardRect.X + playerWidth + leftPadding + leftPadding;
            var y = gameboardRect.Y + yOffset;
            topCenterRect = new Rectangle(left, y, playerWidth, playerHeight);
            gameBoard[1].Rectangle = topCenterRect;
           
            /* top right */
            left = left + playerWidth + leftPadding + xOffset;
            y = gameboardRect.Y + yOffset;
            topRightRect = new Rectangle(left, y, playerWidth, playerHeight);
            gameBoard[2].Rectangle = topRightRect;
             
            /* center left */
            y = gameboardRect.Y + playerHeight + topPadding + topPadding + yOffset;
            centerLeftRect = new Rectangle(gameboardRect.X + xOffset, y + yOffset, playerWidth, playerHeight);
            gameBoard[3].Rectangle = centerLeftRect;

            /* center center */
            left = gameboardRect.X + playerWidth + leftPadding + xOffset + xOffset;
            centercenterRect = new Rectangle(left, y + yOffset, playerWidth, playerHeight);
            gameBoard[4].Rectangle = centercenterRect;

            /* center right */
            left = left + playerWidth + leftPadding + xOffset;
            centerRightRect = new Rectangle(left, y + yOffset, playerWidth, playerHeight);
            gameBoard[5].Rectangle = centerRightRect;
            
            /* bottom left */
            y = gameboardRect.Y + (playerHeight * 2) + (topPadding * 5) + yOffset;
            bottomLeftRect = new Rectangle(gameboardRect.X + xOffset, y, playerWidth, playerHeight);
            gameBoard[6].Rectangle = bottomLeftRect;

            /* bottom center */
            left = gameboardRect.X + playerWidth + leftPadding + xOffset + xOffset;
            bottomCenterRect = new Rectangle(left, y, playerWidth, playerHeight);
            gameBoard[7].Rectangle = bottomCenterRect;

            /* bottom right */
            left = left + playerWidth + leftPadding + xOffset;
            bottomRightRect = new Rectangle(left, y, playerWidth, playerHeight);
            gameBoard[8].Rectangle = bottomRightRect;

        }

        private void DrawGameBoard(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(gameBoardText, gameboardRect, Color.White);
        }

        private void DrawGamePieces(SpriteBatch spriteBatch)
        {
            foreach (var gamePiece in gameBoard)
            {
                if (!gamePiece.IsEmpty)
                {
                    gamePiece.Draw(spriteBatch);
                }
            }
        }

        private void UpdateGamePieces()
        {
            foreach (var gamePiece in gameBoard)
            {
                if (!gamePiece.IsEmpty)
                {
                    gamePiece.Update();
                }
            }
        }

    }
}
