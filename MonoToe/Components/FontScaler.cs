﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoToe.Components
{
    public class FontScaler
    {
        private float _startScale;
        private float _maxScale;
        private float _currentScale;
        private float _scaleSpeed;

        private int _delay;
        private double _startDelay;

        private string _text;
        private Vector2 _position;
        private bool _animating;

        SpriteFont _font;
        Vector2 _fontOrigin;

        public bool Animating
        {
            get
            {
                return _animating;
            }
        }

        public FontScaler(float startScale, 
            float maxScale, 
            float scaleSpeed, 
            Vector2 position,
            int delay, 
            string text)
        {
            _startScale = startScale;
            _maxScale = maxScale;
            _scaleSpeed = scaleSpeed;
            _delay = delay;
            _text = text;
            _currentScale = _startScale;
            _startDelay = 0;
            _position = position;         
        }

        public void LoadContent(SpriteFont font)
        {
            _font = font;

            _fontOrigin = new Vector2(_font.MeasureString(_text).X / 2,
            _font.MeasureString(_text).Y / 2);
        }

        public void Update(GameTime gameTime)
        {
            /* capture the start time */
            if (_startDelay == 0)
                _startDelay = gameTime.TotalGameTime.TotalSeconds;

            /* if there is time left keep scaling the font */
            if ((gameTime.TotalGameTime.TotalSeconds - _startDelay) <= _delay)
            {
                _animating = true;

                if(_currentScale <= _maxScale)
                {
                    _currentScale += _scaleSpeed;
                }
            }
            else
            {
                Reset();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(_font,
                _text,
                _position,
                new Color(91, 235, 0),//Monotoe green
                0,
                _fontOrigin,
                _currentScale,
                SpriteEffects.None,
                0);
        }

        private void Reset()
        {
            _animating = false;
            _startDelay = 0;
            _currentScale = _startScale;
        }
        
    }
}
