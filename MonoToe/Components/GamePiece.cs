﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoToe.Components
{
    public class GamePiece
    {
        public Texture2D _texture;

        public bool IsEmpty
        {
            get
            {
                if (this.Owner == "")
                    return true;
                return false;
            }

        }
        public string Owner { get; set; }
        public Rectangle Rectangle { get; set; }
        public Texture2D Texture
        {
            get
            {
                return _texture;
            }
            set
            {
                _texture = value;
            }
        }
        public Vector2 Position { get; set; }
        public Vector2 Origin { get; set; }
        int scale;
        float rotation;
        int layerDepth;
        public bool IsRotating { get; set; }

        public GamePiece(Rectangle rectangle, string owner)
        {
            var xOffset = 65;
            var yOffset = 65;

            if (rectangle != null)
            {
                Rectangle = rectangle;

                Position = new Vector2(Rectangle.X + xOffset,
                    Rectangle.Y + yOffset);

                Origin = new Vector2(Rectangle.Width / 2,
                    Rectangle.Height / 2);
             
            }
            else
            {
                Rectangle = new Rectangle();
              
            }

            if (string.IsNullOrWhiteSpace(owner))
            {
                Owner = owner;
               
            }
            else
            {
                Owner = "";
            }
            
            scale = 1;
            rotation = 0.0f;
            layerDepth = 0;
        }

        public bool Contains(Point point)
        {
            if (this.Rectangle.Contains(point))
                return true;
            return false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture,
                   Position,
                   null,
                   Color.White,
                   rotation,
                   Origin,
                   scale,
                   SpriteEffects.None,
                   layerDepth);
            }
        }

        public void Update()
        {
            if (IsRotating)
            {
                if (rotation >= 3.142f)
                {
                    EndRotation();
                }
                else
                {
                    rotation += 0.1f;
                }
            }else
            {
               
            }
        }

        public void StartRotation()
        {
            if (!IsRotating)
            {
                IsRotating = true;
                rotation = 0.0f;
            }
        }

        public void EndRotation()
        {
            IsRotating = false;
            //rotation = 0.0f;
        }

    }
}
