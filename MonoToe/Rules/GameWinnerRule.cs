﻿using MonoToe.Components;

namespace MonoToe.Rules
{
    public class GameWinnerRule
    {
     
        //public static bool PlayerWins(GameSession gameSession, GameBoard gameBoard)
        //{
        //   return  GetWinningPlayer(gameBoard).ToUpper();
        //}

        //public static bool CPUWins(GameSession gameSession, GameBoard gameBoard)
        //{
        //    return gameSession.CPUIcon.ToUpper()
        //        == GetWinningPlayer(gameBoard).ToUpper();
        //}

        //public static bool CatWins(GameSession gameSession, GameBoard gameBoard)
        //{
        //    return gameSession.CatIcon.ToUpper()
        //        == GetWinningPlayer(gameBoard).ToUpper();
        //}

        public static string GetWinningPlayer(GameBoard myGameBoard)
        {
            var gameBoard = myGameBoard.gameBoard;

            /* check for the victory conditions and return winning player */

            /* top row */
            if (gameBoard[0].Owner != "" && gameBoard[0].Owner == gameBoard[1].Owner && gameBoard[1].Owner == gameBoard[2].Owner)
               return gameBoard[0].Owner;

            /* middle row */
            if (gameBoard[3].Owner != "" && gameBoard[3].Owner == gameBoard[4].Owner && gameBoard[4].Owner == gameBoard[5].Owner)
               return gameBoard[3].Owner;

            /* bottome row */
            if (gameBoard[6].Owner != "" && gameBoard[6].Owner == gameBoard[7].Owner && gameBoard[7].Owner == gameBoard[8].Owner)
                return gameBoard[6].Owner;

            /* left column */
            if (gameBoard[0].Owner != "" && gameBoard[0].Owner == gameBoard[3].Owner && gameBoard[3].Owner == gameBoard[6].Owner)
                return gameBoard[0].Owner;

            /* middle column */
            if (gameBoard[1].Owner != "" && gameBoard[1].Owner == gameBoard[4].Owner && gameBoard[4].Owner == gameBoard[7].Owner)
                return gameBoard[1].Owner;

            /* right column */
            if (gameBoard[2].Owner != "" && gameBoard[2].Owner == gameBoard[5].Owner && gameBoard[5].Owner == gameBoard[8].Owner)
                return gameBoard[2].Owner;

            /* left diagonal */
            if (gameBoard[0].Owner != "" && gameBoard[0].Owner == gameBoard[4].Owner && gameBoard[4].Owner == gameBoard[8].Owner)
                return gameBoard[0].Owner;

            /* right diagonal */
            if (gameBoard[2].Owner != "" && gameBoard[2].Owner == gameBoard[4].Owner && gameBoard[4].Owner == gameBoard[6].Owner)
                return gameBoard[2].Owner;

            /* All all cells full */
            foreach (var cell in gameBoard)
            {
                if (string.IsNullOrWhiteSpace(cell.Owner))
                    return "";
            }

            /* if we got here there is no winner and the board is full */          
            return "DRAW";
        }
    }
}
