﻿using MonoToe.Components;

namespace MonoToe.Rules
{
    public class GameOverRule
    {
        public static bool IsGameOver(GameBoard gameBoard)
        {
            return !string.IsNullOrWhiteSpace(GameWinnerRule.GetWinningPlayer(gameBoard));
        }
    }
}
